<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Casa
 *
 * @author sushi
 */
require_once 'Construccion.php';
class Casa extends Construccion {
    
    //Atributos de la clase
    
        private $orientacionalsol;
	private $habitaciones;
	private $tipodevivienda;
	private $ubicacion;
        private $pisos;
        
    //Métodos de la clase
        
        public function __construct() {
            parent::__construct();
            $this-> orientacionalsol = "";
            $this-> habitaciones = "";
            $this-> tipodevivienda = "";
            $this-> pisos = "";
            
        }
        
        public function __destruct() {
            parent::__destruct();
        }
        
    //Métodos
    function getOrientacionalsol() {
        return $this->orientacionalsol;
    }

    function getHabitaciones() {
        return $this->habitaciones;
    }

    function getTipodevivienda() {
        return $this->tipodevivienda;
    }

    function getPisos() {
        return $this->pisos;
    }

    function setOrientacionalsol($orientacionalsol) {
        $this->orientacionalsol = $orientacionalsol;
    }

    function setHabitaciones($habitaciones) {
        $this->habitaciones = $habitaciones;
    }

    function setTipodevivienda($tipodevivienda) {
        $this->tipodevivienda = $tipodevivienda;
    }


    function setPisos($pisos) {
        $this->pisos = $pisos;
    }


                        
    
    
}
