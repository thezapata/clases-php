<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Objetos
 *
 * @author sushi
 */
class Objetos {
    
//Atributos de clase
    
    	private $longitud;
	private $altura;
	private $ancho;
	private $color;
	private $perimetro;
	private $volumen;
        private $zona;
        private $pisos;

//Métodos de la clase
       public function __construct() {
           
           $this-> longitud= 0;
           $this-> altura = 0;
           $this-> ancho = 2;
           $this-> color = "Blanco";
           $this-> perimetro = 0;
           $this-> volumen = 0;
           $this-> zona = "";
           $this-> pisos = 0;
       }
       
        public function __destruct() {
           
        }

//Get and set
    function getLongitud() {
        return $this->longitud;
    }

    function getAltura() {
        return $this->altura;
    }

    function getAncho() {
        return $this->ancho;
    }

    function getColor() {
        return $this->color;
    }

    function getPerimetro() {
        return $this->perimetro;
    }

    function getVolumen() {
        return $this->volumen;
    }

    function getZona() {
        return $this->zona;
    }

    function setLongitud($longitud) {
        $this->longitud = $longitud;
    }

    function setAltura($altura) {
        $this->altura = $altura;
    }

    function setAncho($ancho) {
        $this->ancho = $ancho;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setPerimetro($perimetro) {
        $this->perimetro = $perimetro;
    }

    function setVolumen($volumen) {
        $this->volumen = $volumen;
    }

    function setZona($zona) {
        $this->zona = $zona;
    }
    function getPisos() {
        return $this->pisos;
    }

    function setPisos($pisos) {
        $this->pisos = $pisos;
    }

//Método
    
    public function areaObjeto($altura, $ancho){
        
        $this-> resultado = $ancho * $altura; 
        
    }
    
    public function zonaEdificacion($zona){
        switch ($zona){
             case "ciudad";
                $this->zona = "urbana";
                break;
            case "campo";
                $this->zona = "rural";
                break;
        }
    }
    
}

    
