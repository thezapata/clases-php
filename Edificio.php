<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Edificio
 *
 * @author sushi
 */
require_once 'Construccion.php'; 
class Edificio extends Construccion

{
    //Atributos de la clase
    
	private $mododeuso;
	private $material;
	private $tipodeiluminacion;
       

	//Métodos de la clase

	public function __construct() {
            parent::__construct();
            $this-> mododeuso = "";
            $this-> material = "";
            $this-> tipodeiluminacion = "";
            
            
        }

	public function __destruct()
	{
            parent::__destruct();
		
	}

    //Get and set
    function getMododeuso() {
        return $this->mododeuso;
    }

    function getMaterial() {
        return $this->material;
    }

    function getTipodeiluminacion() {
        return $this->tipodeiluminacion;
    }

    function setMododeuso($mododeuso) {
        $this->mododeuso = $mododeuso;
    }

    function setMaterial($material) {
        $this->material = $material;
    }

    function setTipodeiluminacion($tipodeiluminacion) {
        $this->tipodeiluminacion = $tipodeiluminacion;
    }

}
    
    